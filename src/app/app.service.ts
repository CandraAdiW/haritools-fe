import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, catchError, map,Observable } from 'rxjs';
import { environment } from "../environments/environment";

export interface ShareOptionData {
  indexHalamanHapus: number,
  listFileDownload: string[],
  typeDownload: string
}

export interface StatusProses{
  persen : number,
  status : string,
  spin   : boolean,
  isLoading : boolean,
  dataDownload? : any
}

export declare type statusHalaman = "upload" | "statusUpload" | "listFile" | "download" | "proses" | "showText";

@Injectable({
  providedIn: 'root'
})
export class AppService {
  httpOptions: any = [];
  dataFile = new BehaviorSubject([]);
  currentDataFile = this.dataFile.asObservable();

  optionDataUpload: StatusProses = { persen: 0, status: 'upload', spin: true,isLoading : false};
  isDataUpload = new BehaviorSubject(this.optionDataUpload);
  currentIsDataUpload = this.isDataUpload.asObservable();


  optionDataSubject: ShareOptionData = { indexHalamanHapus: -1, listFileDownload: [], typeDownload: '' };
  dataOption = new BehaviorSubject(this.optionDataSubject);
  currentHalamanOption = this.dataOption.asObservable();
  eventSource!: EventSource;

  constructor(
    private http: HttpClient,
    private zone: NgZone,
    private _snackBar: MatSnackBar
  ) { }

  post(url: string, body: any, reponse: any = { responseType: 'json' }): Observable<any> {
    let dataBody = body;
    reponse["reportProgress"] = true;
    reponse["observe"] = "events";
    return this.http.post<any>(
      environment.apiUrl + url,
      dataBody, reponse
    ).pipe(
      map((res) => {
        return res;
      }),
      catchError(err => {
        return err;
      })
    );
  }

  postDownload(url: string, body: any, reponse: any = { responseType: 'blob' }): Observable<any> {
    let dataBody = body;
    return this.http.post<any>(
      environment.apiUrl + url,
      dataBody, reponse
    ).pipe(
      map((res) => {
        return res;
      }),
      catchError(err => {
        return err;
      })
    );
  }

  get(url: string, param: any): Observable<any> {
    var dataParam = "?"
    for (let i in param) {
      dataParam += (dataParam.length == 1 ? "" : "&") + i + "=" + param[i]
    }

    return this.http.get<any>
      (environment.apiUrl + url + dataParam, this.httpOptions).pipe(
        map((res) => {
          return res;
        }),
        catchError(err => {
          return err;
        })
      );
  };

  getAlamatLengkap(url: string, param: any): Observable<any> {
    var dataParam = "?"
    for (let i in param) {
      dataParam += (dataParam.length == 1 ? "" : "&") + i + "=" + param[i]
    }

    return this.http.get<any>
      (url + dataParam, this.httpOptions).pipe(
        map((res) => {
          return res;
        }),
        catchError(err => {
          return err;
        })
      );
  };

  sseStream(url: string): EventSource {
    return new EventSource(environment.apiUrl + url);
  }

  getSseService(pathUrl: string) {
    return new Observable(observer => {
      this.eventSource = this.sseStream(pathUrl);

      this.eventSource.onmessage = event => {
        this.zone.run(() => {
          observer.next(event.data);
        })
      }

      this.eventSource.onerror = error => {
        this.zone.run(() => {
          this.eventSource.close();
          observer.error(error);
        })
      }

    })
  }

  sharedDataFile(data: any) {
    this.dataFile.next(data);
  }

  sharedOptionData(data: ShareOptionData) {
    this.dataOption.next(data);
  }

  sharedInfoDownlaod(data: StatusProses) {
    this.isDataUpload.next(data);
  }

  openSnackBar(message: string, buttonText: string) {
    this._snackBar.open(message, buttonText, {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 10 * 1000
    });
  }

  destroyObservable(){
    this.isDataUpload.next(this.optionDataUpload);
    this.dataFile.next([]);
    this.dataOption.next(this.optionDataSubject);
  }

}
