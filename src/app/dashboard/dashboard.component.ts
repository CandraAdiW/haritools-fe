import { Component, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { MenuItems } from '../shared/menu-items/menu-items';
import { Meta, Title } from '@angular/platform-browser';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {

  constructor(
    public menuItems: MenuItems,
    private title: Title,
    private meta: Meta
  ) {
  }

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
    this.title.setTitle("Haritools - Home");
    this.meta.addTags([
      { name: "description", content: "Haritools merupakan aplikasi online gratis (free) untuk melakukan perubahan file PDF, Gambar, Dokumen Office, dan masih banyak lagi !" },
      {name : "author", content :"haritools.com"},
      {name:"keyword", content: "Json View, Split PDF, Extract Image from Office Document, Extract Image from PDF, Image to PDF, PDF to Office Document, Compress Image, Compress PDF, Image to Base64, Extract Image Metadata"},
      {property : "og:site_name", content :"Haritools - Online Tools"},
      {property : "og:title", content :"Haritools | Online Tools"},
      { property: "og:description", content:"Haritools merupakan aplikasi online gratis (free) untuk melakukan perubahan file PDF, Gambar, Dokumen Office, dan masih banyak lagi !"},
      { property: "og:type", content: "website" },
      { property: "og:url", content: "https://haritools.com" },
      { charset: "UTF-8" }
    ]);
  }

}
