import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Meta, Title } from '@angular/platform-browser';
import { AppService } from '../../../app.service';

@Component({
  selector: 'app-json-view',
  templateUrl: './json-view.component.html',
  styleUrls: ['./json-view.component.scss']
})
export class JsonViewComponent implements OnInit {
  formInsert!: FormGroup;
  selectActiveIndex : number = 0;
  tmpDataJson : any = [];

  constructor(
    private title     : Title,
    private meta      : Meta,
    private fb        : FormBuilder,
    private cdr       : ChangeDetectorRef,
    private AppService: AppService,
  ) {
  }

  ngOnInit(): void {
    this.title.setTitle("Haritools - Json View");
    this.meta.updateTag({ name: "description", content: "Copy Paste atau Tulis format Json yang akan di proses/perlihatkan berbentuk tree " });
    this.meta.updateTag({property: "og:description", content: "Copy Paste atau Tulis format Json yang akan di proses/perlihatkan berbentuk tree"});
    this.meta.updateTag({ property: "og:title", content: "Haritools | View Structured Json" });
    this.meta.updateTag({ property: "og:url", content: "https://haritools.com/json-view"});
    this.initForm();
  }


  initForm(){
    this.formInsert = this.fb.group({
      dataJson : ["",Validators.required]
    });
  }

  onTabChanged(event : any){
    const controls = this.formInsert.controls;
    if(this.formInsert.invalid){
      this.selectActiveIndex = 0;
      this.cdr.markForCheck();
      this.AppService.openSnackBar("Data Harus Diisi","Ok");
    }else{
      try{
        this.tmpDataJson = JSON.parse(JSON.parse(JSON.stringify( controls["dataJson"].value)));
      }catch(e){
        this.selectActiveIndex = 0;
        this.AppService.openSnackBar("Data Json Tidak Valid", "Ok");
      }
      this.cdr.markForCheck();
    }


  }

}
