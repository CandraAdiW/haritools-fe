import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocExImageComponent } from './doc-ex-image.component';

describe('DocExImageComponent', () => {
  let component: DocExImageComponent;
  let fixture: ComponentFixture<DocExImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocExImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocExImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
