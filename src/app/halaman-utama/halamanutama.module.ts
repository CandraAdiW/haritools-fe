import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TooltipComponent } from './tooltip/tooltip.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { SharedModule } from '../shared/shared.module';
import { ClipboardModule } from 'ngx-clipboard';
import { DocExImageComponent } from './doc-tools/doc-ex-image/doc-ex-image.component';
import { ExtractimageComponent } from './pdf-tools/extractimage/extractimage.component';
import { PdfCompressComponent } from './pdf-tools/pdf-compress/pdf-compress.component';
import { PdfToDocComponent } from './pdf-tools/pdf-to-doc/pdf-to-doc.component';
import { SplitComponent } from './pdf-tools/split/split.component';
import { ImageCompressComponent } from './image-tools/image-compress/image-compress.component';
import { ImageToPdfComponent } from './image-tools/image-to-pdf/image-to-pdf.component';
import { JsonViewComponent } from './dev-tools/json-view/json-view.component';
import { ImageToBase64Component } from './image-tools/image-to-base64/image-to-base64.component';
import { ImageMetadataComponent } from './image-tools/image-metadata/image-metadata.component';



const HalamanutamaRoutes: Routes = [
  {
    path: 'tooltip',
    component: TooltipComponent
  },
  {
    path: 'doc-extract-image',
    component: DocExImageComponent
  },
  {
    path: 'extract-image',
    component: ExtractimageComponent
  },
  {
    path: 'pdf-compress',
    component: PdfCompressComponent
  },
  {
    path: 'pdf-to-doc',
    component: PdfToDocComponent
  },
  {
    path: 'split-pdf',
    component: SplitComponent
  },
  {
    path: 'image-compress',
    component: ImageCompressComponent
  },
  {
    path: 'image-to-pdf',
    component: ImageToPdfComponent
  },
  {
    path: 'json-view',
    component: JsonViewComponent
  },
  {
    path: 'image-to-base64',
    component: ImageToBase64Component
  },
  {
    path: 'image-metadata',
    component: ImageMetadataComponent
  }
];


@NgModule({
  imports: [
    SharedModule,
    DragDropModule,
    CommonModule,
    RouterModule.forChild(HalamanutamaRoutes),
    DemoMaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    CdkTableModule,
    NgxJsonViewerModule,
    MatInputModule,
    ClipboardModule,
    MatButtonModule
  ],
  providers: [],
  entryComponents: [],
  declarations: [
    TooltipComponent,
    DocExImageComponent,
    ExtractimageComponent,
    PdfCompressComponent,
    PdfToDocComponent,
    SplitComponent,
    ImageCompressComponent,
    ImageToPdfComponent,
    JsonViewComponent,
    ImageToBase64Component,
    ImageMetadataComponent
  ]
})
export class HalamanutamaComponentsModule {}
