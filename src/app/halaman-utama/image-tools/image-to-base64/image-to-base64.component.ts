import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AppService, StatusProses, statusHalaman } from '../../../app.service';

@Component({
  selector: 'app-image-to-base64',
  templateUrl: './image-to-base64.component.html',
  styleUrls: ['./image-to-base64.component.scss']
})
export class ImageToBase64Component implements OnInit {
  subscriptionData$!: Subscription;
  hasilProses: string[] = [];
  statusHalaman: statusHalaman = "upload";

  constructor(
    private title: Title,
    private meta: Meta,
    private AppService: AppService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Haritools - Image to Base 64");
    this.meta.updateTag({ name: "description", content: "Unggah file gambar untuk diubah menjadi string encode Base64. Konversi gambar ke base 64 mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:description", content: "Unggah file gambar untuk diubah menjadi string encode Base64. Konversi gambar ke base 64 mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:title", content: "Haritools | Image to Base 64" });
    this.meta.updateTag({ property: "og:url", content: "https://haritools.com/image-to-base64" });

    this.subscriptionData$ = this.AppService.currentDataFile.subscribe(itemData => this.uploadFile(itemData));
  }

  setIsUpload(data: StatusProses) {
    this.statusHalaman = <statusHalaman>data.status;
    this.cdr.markForCheck();
  }

  async uploadFile(data: any) {
    if (data.length > 0) {
      const reader = new FileReader();
      let hasil = await this.toBase64(data[0]);
      this.hasilProses.push(<string>hasil);
      this.statusHalaman = "showText";
      this.cdr.markForCheck();
    }
  }

  toBase64 = (file: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  copied(event : any){
    if (event.isSuccess){
      this.AppService.openSnackBar("Data berhasil di copy","Ok");
    }
  }

  ngOnDestroy() {
    this.subscriptionData$.unsubscribe();
    this.AppService.destroyObservable();
  }

}
