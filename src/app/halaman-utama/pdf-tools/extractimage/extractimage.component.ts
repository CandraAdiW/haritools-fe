import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AppService, StatusProses, statusHalaman } from '../../../app.service';

@Component({
  selector: 'app-extractimage',
  templateUrl: './extractimage.component.html',
  styleUrls: ['./extractimage.component.scss']
})
export class ExtractimageComponent implements OnInit {
  statusHalaman: statusHalaman = "upload";
  hasilProses: string[] = [];
  halamanDiHapus: number[] = [];
  subscriptionData$!: Subscription;
  subscriptionisUpload$!: Subscription;
  typeDownload: string = 'image';
  dataDownload: any;

  constructor(
    private title: Title,
    private meta: Meta,
    private AppService: AppService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Haritools - Pdf Extract Image");
    this.meta.updateTag({ name: "description", content: "Unggah file pdf untuk mendapatkan seluruh gambar yang terkandung dalam file. Extract file gambar dari dokumen PDF, mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:description", content: "Unggah file pdf untuk mendapatkan seluruh gambar yang terkandung dalam file. Extract file gambar dari dokumen PDF, mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:title", content: "Haritools | Pdf Extract Image" });
    this.meta.updateTag({ property: "og:url", content: "https://haritools.com/extract-image" });

    this.subscriptionData$ = this.AppService.currentDataFile.subscribe(itemData => this.uploadFile(itemData));
    this.subscriptionisUpload$ = this.AppService.currentIsDataUpload.subscribe(item => this.setIsUpload(item));
  }


  setIsUpload(data: StatusProses) {
    this.statusHalaman = <statusHalaman>data.status;
    if (this.statusHalaman == 'download') {
      this.dataDownload = data.dataDownload;
    }
    this.cdr.markForCheck();
  }

  async uploadFile(data: any) {
    if (data.length > 0) {
      this.statusHalaman = "statusUpload";
      this.cdr.markForCheck();
      let paramFormData = new FormData();
      data.forEach((item: string | Blob) => {
        paramFormData.append("file", item);
      });
      this.AppService.post("/extract-image-pdf", paramFormData).subscribe((event: HttpEvent<any>) => {
        let tmpData: StatusProses;
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            tmpData = { persen: 0, status: 'statusUpload', spin: true, isLoading: true };
            this.AppService.sharedInfoDownlaod(tmpData);
            break;
          case HttpEventType.UploadProgress:
            let progress = Math.round(event.loaded / (event.total ? event.total : 1) * 100);
            tmpData = { persen: progress, status: 'statusUpload', spin: true, isLoading: true };
            this.AppService.sharedInfoDownlaod(tmpData);
            break;
          case HttpEventType.Response:
            if (typeof event.body != 'undefined') {
              this.hasilProses = event.body["data"][0]["process"];
              tmpData = { persen: 100, status: 'listFile', spin: true, isLoading: false };
              this.AppService.sharedInfoDownlaod(tmpData);
              this.AppService.sharedDataFile([]);
              this.cdr.markForCheck();
            }
        }
      });
    }
  }

  ngOnDestroy() {
    this.subscriptionData$.unsubscribe();
    this.subscriptionisUpload$.unsubscribe();
    this.AppService.destroyObservable();
  }
}
