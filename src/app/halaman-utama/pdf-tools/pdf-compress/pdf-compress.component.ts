import { HttpEvent, HttpEventType } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AppService, StatusProses, statusHalaman } from '../../../app.service';

@Component({
  selector: 'app-pdf-compress',
  templateUrl: './pdf-compress.component.html',
  styleUrls: ['./pdf-compress.component.scss']
})
export class PdfCompressComponent implements OnInit {
  subscriptionData$!: Subscription;
  subscriptionisUpload$!: Subscription;
  hasilProses!: string[];
  typeDownload: string = 'pdf';
  statusHalaman: statusHalaman = "upload";
  dataDownload: any;

  constructor(
    private title: Title,
    private meta: Meta,
    private AppService: AppService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.title.setTitle("Haritools - PDF Compress");
    this.meta.updateTag({ name: "description", content: "Unggah file pdf untuk memperkecil ukuran file. Kompres ukuran file pdf, mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:description", content: "Unggah file pdf untuk memperkecil ukuran file. Kompres ukuran file pdf, mudah dan gratis (Free)" });
    this.meta.updateTag({ property: "og:title", content: "Haritools | PDF Compress" });
    this.meta.updateTag({ property: "og:url", content: "https://haritools.com/pdf-compress" });

    this.subscriptionData$ = this.AppService.currentDataFile.subscribe(itemData => this.uploadFile(itemData));
    this.subscriptionisUpload$ = this.AppService.currentIsDataUpload.subscribe(item => this.setIsUpload(item));
  }

  setIsUpload(data: StatusProses) {
    this.statusHalaman = <statusHalaman>data.status;
    if (this.statusHalaman == 'download') {
      this.dataDownload = data.dataDownload;
    }
    this.cdr.markForCheck();
  }
  async uploadFile(data: any) {
    if (data.length > 0) {
      this.statusHalaman = "statusUpload";
      this.cdr.markForCheck();
      let paramFormData = new FormData();
      data.forEach((item: string | Blob) => {
        paramFormData.append("file", item);
      });
      this.AppService.post("/pdf-compress", paramFormData).subscribe((event: HttpEvent<any>) => {
        let tmpData: StatusProses;
        switch (event.type) {
          case HttpEventType.Sent:
            console.log('Request has been made!');
            break;
          case HttpEventType.ResponseHeader:
            tmpData = { persen: 0, status: 'statusUpload', spin: true, isLoading: true };
            this.AppService.sharedInfoDownlaod(tmpData);
            break;
          case HttpEventType.UploadProgress:
            let progress = Math.round(event.loaded / (event.total ? event.total : 1) * 100);
            tmpData = { persen: progress, status: 'statusUpload', spin: true, isLoading: true };
            this.AppService.sharedInfoDownlaod(tmpData);
            break;
          case HttpEventType.Response:
            if (typeof event.body != 'undefined') {
              this.hasilProses = event.body["data"][0]["process"];
              let tmpDataDownload = {
                fileList: this.hasilProses.map(item => "fileProses" + item.split("fileProses")[1]),
                type: this.typeDownload
              }
              tmpData = { persen: 100, status: 'download', spin: true, isLoading: false, dataDownload: tmpDataDownload };
              this.AppService.sharedInfoDownlaod(tmpData);
              this.AppService.sharedDataFile([]);
              this.cdr.markForCheck();
            }
        }
      });
    }
  }


  ngOnDestroy() {
    this.subscriptionData$.unsubscribe();
    this.subscriptionisUpload$.unsubscribe();
    this.AppService.destroyObservable();
  }


}
