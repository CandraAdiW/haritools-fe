import { Component, Input, OnInit } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { AppService, StatusProses } from '../../app.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {
  @Input() paramDonwload : any;
  constructor(
    private AppService: AppService,
  ) { }

  ngOnInit(): void {
  }

  async download(){
    let tmpData: StatusProses = { persen: 100, status: 'proses', spin: true, isLoading: false  };
    this.AppService.sharedInfoDownlaod(tmpData);
    const hasilUp$ = this.AppService.postDownload("/download", this.paramDonwload, { responseType: 'blob' });
    let downloadFile = await firstValueFrom(hasilUp$);
    let blob = new Blob([downloadFile], { type: downloadFile.type });
    saveAs(blob, 'download.zip');
    tmpData = { persen: 100, status: 'download', spin: true, isLoading: false, dataDownload: this.paramDonwload};
    this.AppService.sharedInfoDownlaod(tmpData);
  }
}
