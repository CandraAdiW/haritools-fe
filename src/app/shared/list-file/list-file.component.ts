import { ChangeDetectorRef, Component, ElementRef, Input, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { environment } from '../../../environments/environment';
import { NgZoom } from 'ng-zoom';
import { SelectionModel } from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { AppService } from '../../app.service';
import {
  CdkDragDrop,
  CdkDragEnter,
  CdkDragMove,
  moveItemInArray,
} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-list-file',
  templateUrl: './list-file.component.html',
  styleUrls: ['./list-file.component.scss']
})
export class ListFileComponent implements OnInit {
  @ViewChild('dropListContainer') dropListContainer?: ElementRef;
  @ViewChildren("imageEl")
  imagesRef!: ElementRef<HTMLImageElement>[];
  @Input() listFile! : string[];
  @Input() typeDownload! : string;
  envApi: string = environment.apiUrl ;
  selection = new SelectionModel<any>(true, []);
  dataOptionKirim$: Subscription = new Subscription;
  checked : number = 0;
  dropListReceiverElement?: HTMLElement;
  dragDropInfo?: {
    dragIndex: number;
    dropIndex: number;
  };


  constructor(
    private AppService  : AppService,
    private cdr         : ChangeDetectorRef,
    private ngz         : NgZoom
  ) { }

  ngOnInit(): void {
    this.listFile = this.listFile.map(item => this.envApi +item);
    this.AppService.sharedOptionData(
      {
        indexHalamanHapus: -1,
        listFileDownload: this.listFile,
        typeDownload: this.typeDownload
      }
    );
    this.listFile.forEach((item,index) => {
      this.checkMe(index,'listData');
    })
    this.cdr.markForCheck();
  }


  ngAfterViewInit() {
    this.imagesRef.forEach((imageRef: ElementRef<HTMLImageElement>) => {
      this.ngz.listen(imageRef);
    });
  }


  checkMe(index : any,asalData : string = "listHtml"){
    if (asalData == "listHtml"){
      this.AppService.sharedOptionData(
        {
          indexHalamanHapus : index + 1,
          listFileDownload  : this.listFile,
          typeDownload      : this.typeDownload
        }
      );
    }
    this.selection.toggle(index);
  }

  dragEntered(event: CdkDragEnter<number>) {
    const drag = event.item;
    const dropList = event.container;
    const dragIndex = drag.data;
    const dropIndex = dropList.data;

    this.dragDropInfo = { dragIndex, dropIndex };
    console.log('dragEntered', { dragIndex, dropIndex });

    const phContainer = dropList.element.nativeElement;
    const phElement = phContainer.querySelector('.cdk-drag-placeholder');

    if (phElement) {
      phContainer.removeChild(phElement);
      phContainer.parentElement?.insertBefore(phElement, phContainer);

      moveItemInArray(this.listFile, dragIndex, dropIndex);
    }
  }

  dragMoved(event: CdkDragMove<number>) {
    if (!this.dropListContainer || !this.dragDropInfo) return;

    const placeholderElement =
      this.dropListContainer.nativeElement.querySelector(
        '.cdk-drag-placeholder'
      );

    const receiverElement =
      this.dragDropInfo.dragIndex > this.dragDropInfo.dropIndex
        ? placeholderElement?.nextElementSibling
        : placeholderElement?.previousElementSibling;

    if (!receiverElement) {
      return;
    }

    receiverElement.style.display = 'none';
    this.dropListReceiverElement = receiverElement;
  }

  dragDropped(event: CdkDragDrop<number>) {
    if (!this.dropListReceiverElement) {
      return;
    }

    this.dropListReceiverElement.style.removeProperty('display');
    this.dropListReceiverElement = undefined;
    this.dragDropInfo = undefined;
  }

  ngOnDestroy() {
    this.dataOptionKirim$.unsubscribe();
  }
}
