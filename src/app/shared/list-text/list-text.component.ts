import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-text',
  templateUrl: './list-text.component.html',
  styleUrls: ['./list-text.component.scss']
})
export class ListTextComponent implements OnInit {
  @Input() data! : string[];

  constructor() { }

  ngOnInit(): void {
  }

  splitData(data : any, aksi : string){
    let tmp = data.split(":");
    if(tmp.length > 1){
      if (aksi == 'label') {
        return tmp[0];
      } else {
        return tmp[1];
      }
    }
  }

}
