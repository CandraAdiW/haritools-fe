import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  url?: string;
  kategori?:string;
  deskripsi?:string;
}

const MENUITEMS = [
  {
    state: 'dashboard',
    name: 'Dashboard',
    type: 'link',
    url: 'dashboard',
    icon: 'home'
  },
  {
    state: 'json',
    name: 'Json View',
    type: 'link',
    icon: 'format_paint',
    url: '/json-view',
    kategori: 'Dev Tools',
    deskripsi:'Melakukan format input data JSON.'
  },
  {
    state: 'pdf',
    name: 'Split PDF',
    type: 'link',
    icon: 'format_paint',
    url: '/split-pdf',
    kategori: 'Pdf Tools',
    deskripsi: 'Melakukan Pemisahan Multi Halaman File PDF.'
  },
  {
    state: 'doc',
    name: 'Extract Doc Image',
    type: 'link',
    icon: 'open_in_new',
    url: '/doc-extract-image',
    kategori: 'Doc Tools',
    deskripsi: 'Melakukan Pemisahan gambar file doc, docx.'
  },
  {
    state: 'pdf',
    name: 'Extract PDF Image',
    type: 'link',
    icon: 'open_in_new',
    url: '/extract-image',
    kategori: 'Pdf Tools',
    deskripsi: 'Melakukan Pemisahan gambar pada file PDF.'
  },
  {
    state: 'image',
    name: 'Image to PDF',
    type: 'link',
    icon: 'swap_vert',
    url: '/image-to-pdf',
    kategori: 'Image Tools',
    deskripsi: 'Pembuatan file gambar ke pdf.'
  },
  {
    state: 'pdf',
    name: 'PDF to Doc',
    type: 'link',
    icon: 'swap_vert',
    url: '/pdf-to-doc',
    kategori: 'PDF Tools',
    deskripsi: 'Pembuatan file PDF to Docx.'
  },
  {
    state: 'image',
    name: 'Image Compress',
    type: 'link',
    icon: 'reduce_capacity',
    url: '/image-compress',
    kategori: 'Image Tools',
    deskripsi: 'Proses memperkecil ukuran file gambar.'
  },
  {
    state: 'pdf',
    name: 'PDF Compress',
    type: 'link',
    icon: 'reduce_capacity',
    url: '/pdf-compress',
    kategori: 'PDF Tools',
    deskripsi: 'Proses memperkecil ukuran file PDF.'
  },
  {
    state: 'image',
    name: 'Image to Base64',
    type: 'link',
    icon: 'transform',
    url: '/image-to-base64',
    kategori: 'Image Tools',
    deskripsi: 'Merubah file gambar ke bentuk encoding base 64.'
  },
  {
    state: 'image',
    name: 'Image Metadata',
    type: 'link',
    icon: 'transform',
    url: '/image-metadata',
    kategori: 'Image Tools',
    deskripsi: 'Mengambil metadata dalam file gambar.'
  }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
