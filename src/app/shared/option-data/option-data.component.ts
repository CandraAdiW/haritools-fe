import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { firstValueFrom } from 'rxjs';
import { AppService, ShareOptionData, StatusProses } from '../../app.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-option-data',
  templateUrl: './option-data.component.html',
  styleUrls: ['./option-data.component.scss']
})
export class OptionDataComponent implements OnInit {
  formFilter!: FormGroup;
  tmpDataIndexHalamanHapus : number[] = [];
  tmpDataOption!: ShareOptionData;

  constructor(
    private AppService : AppService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.AppService.currentHalamanOption.subscribe(itemData => this.getData(itemData));
    this.initForm();
  }

  initForm() {
    this.formFilter = this.formBuilder.group({
      halamanHapus: [""]
    });
  }

  setData(data : ShareOptionData){
    this.tmpDataOption = data;
  }

  getData(data: ShareOptionData){
    this.tmpDataOption = data;
    if (data.indexHalamanHapus != -1){
      let idx = this.tmpDataIndexHalamanHapus.findIndex(item => item == data.indexHalamanHapus);
      if (idx != -1) {
        //kalo ada berarti batal (di check lagi)
        this.tmpDataIndexHalamanHapus.splice(idx,1);
      } else {
        //kalo dak ada push
        this.tmpDataIndexHalamanHapus.push(data.indexHalamanHapus);
      }
      this.formFilter.patchValue({
        halamanHapus: this.tmpDataIndexHalamanHapus.sort()
      });
    }
  }

  filterData(item : string, index : number){
    let hasil = "-";
    let idx = this.tmpDataIndexHalamanHapus.findIndex(item => item == index+1);
    if(idx == -1 ){
      hasil = item;
    }
    return hasil;
  }


  prosesData(){
    if (this.tmpDataIndexHalamanHapus.length > 0) {
      this.tmpDataOption.listFileDownload = this.tmpDataOption.listFileDownload.map((item, index) => this.filterData(item, index)).filter(item => item != '-');
    }
    let paramDonwload = {
      fileList  : this.tmpDataOption.listFileDownload.map(item => "fileProses" + item.split("fileProses")[1]),
      type      : this.tmpDataOption.typeDownload
    }

    let tmpData = { persen: 100, status: 'download', spin: true, isLoading: false, dataDownload: paramDonwload };
    this.AppService.sharedInfoDownlaod(tmpData);
  }

  async downloadData(){

  }

  // async downloadData(){
  //   //this.AppService.sharedInfoDownlaod(true);
  //   if (this.tmpDataIndexHalamanHapus.length > 0){
  //     this.tmpDataOption.listFileDownload = this.tmpDataOption.listFileDownload.map((item, index) => this.filterData(item,index)).filter(item => item != '-');
  //   }
  //   let paramDonwload = {
  //     fileList: this.tmpDataOption.listFileDownload.map(item => "fileProses" + item.split("fileProses")[1]),
  //     type    : this.tmpDataOption.typeDownload
  //   }
  //   const hasilUp$ = this.AppService.post("/download", paramDonwload, { responseType: 'blob' });
  //   let downloadFile = await firstValueFrom(hasilUp$);
  //   let blob = new Blob([downloadFile], { type: downloadFile.type });
  //   //this.AppService.sharedInfoDownlaod(false);
  //   saveAs(blob, 'download.zip');
  // }

}
