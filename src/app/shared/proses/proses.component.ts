import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { Subscription } from 'rxjs';
import { AppService, StatusProses } from '../../app.service';

@Component({
  selector: 'app-proses',
  templateUrl: './proses.component.html',
  styleUrls: ['./proses.component.scss']
})
export class ProsesComponent implements OnInit {
  @Input() mode: ProgressSpinnerMode = 'determinate';//indeterminate
  color: ThemePalette = 'primary';
  value : number = 50;
  status : string = "";

  subscriptionisUpload$!: Subscription;

  constructor(
    private AppService: AppService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.subscriptionisUpload$ = this.AppService.currentIsDataUpload.subscribe(item => this.setIsUpload(item));
  }

  setIsUpload(data: StatusProses) {
    this.value  = data.persen;
    this.status = data.status;
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptionisUpload$.unsubscribe();
  }

}
