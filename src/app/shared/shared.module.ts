import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CdkTableModule } from '@angular/cdk/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { MatListModule } from '@angular/material/list';
import { MenuItems } from './menu-items/menu-items';
import {UploadComponent} from './upload/upload.component';
import {ListFileComponent} from './list-file/list-file.component';
import { OptionDataComponent } from './option-data/option-data.component';
import { ProsesComponent } from './proses/proses.component';
import { DownloadComponent } from './download/download.component';
import { ListTextComponent } from './list-text/list-text.component';

@NgModule({
  declarations: [
    UploadComponent,
    ListFileComponent,
    OptionDataComponent,
    ProsesComponent,
    DownloadComponent,
    ListTextComponent
  ],
  exports: [
    UploadComponent,
    ListFileComponent,
    OptionDataComponent,
    ProsesComponent,
    DownloadComponent,
    ListTextComponent
  ],
  imports: [
    CdkTableModule,
    MatButtonModule,
    CommonModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    NgxDropzoneModule,
    MatProgressSpinnerModule,
    MatListModule
  ],
  providers: [ MenuItems ]
})
export class SharedModule { }
