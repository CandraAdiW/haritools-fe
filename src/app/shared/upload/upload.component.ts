import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { firstValueFrom, Subscription } from 'rxjs';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @Input() mimeType! : string;
  files: File[] = [];
  dataFileKirim$: Subscription = new Subscription;
  acceptMime : string="";
  sizeLimit : number = 10;//dalam MB (default)

  constructor(
    private AppService: AppService,
    private router : Router,
    private cdr : ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getDataValidate();
  }

  async getDataValidate(){
    const dataValidate$ = this.AppService.get("/mime-list",{});
    let hasil = await firstValueFrom(dataValidate$);
    try{
      let idx = hasil["data"].findIndex((item: { type: string; })  => item.type == this.mimeType);
      if (idx !=-1){
        this.acceptMime = hasil["data"][idx]["mime"].join(",");
        this.cdr.markForCheck();
      }else{
        this.AppService.openSnackBar("Not Valid MimeType !", "OK");
        throw new Error("Not Valid MimeType !");
      }
    }catch(e){
      this.router.navigate(["/home"]);
    }
  }

  onSelect(event: any) {
    this.files = [];
    this.files.push(...event.addedFiles);
    if (event.addedFiles.length == 0){
      this.AppService.openSnackBar("File Not Valid !", "OK");
    }else{
      if (event.addedFiles[0].size <= (this.sizeLimit * 1000000)) {
        this.AppService.sharedDataFile(this.files);
      } else {
        this.AppService.openSnackBar("File to Large", "OK");
      }
    }
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  ngOnDestroy() {
    this.dataFileKirim$.unsubscribe();
  }

}
