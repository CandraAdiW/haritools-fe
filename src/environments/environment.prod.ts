export const environment = {
  production: true,
  apiUrl: "https://api.haritools.com",
  hostUrl: "http://localhost:4200",
};
